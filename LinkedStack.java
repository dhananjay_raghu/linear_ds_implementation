public class LinkedStack<T>
{
    Node head;
    public LinkedStack(){
        head = null;
    }
    public void push(T value){
        Node newNode = new Node(value);
        if (head != null) {
            newNode.setNext(head);
        }
        head = newNode;
        return;
    }
    public T pop(){
        Node temp = head;
        head = head.getNext();
        return (T) temp.getValue();


    }
    public T peak(){
        return (T) head.getValue();
    }

}
