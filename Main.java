public class Main {
    public static void main(String [] args){
        LinkedList<java.io.Serializable> a = new LinkedList<>();
        Node c = a.addNode("Hi");
        Node b = a.addNode(7);
        Node d = a.addNode(8);
        Node e = a.addNode(17.05);
       a.printList();
        reverseList(c);
        System.out.println("-----------------------------");
        a.printReverseList();
//        LinkedQueue<java.io.Serializable> queue = new LinkedQueue<>();
//        queue.enqueue("test");
//        queue.enqueue(5);
//        queue.enqueue(17.42);
//        queue.dequeue();
//        queue.dequeue();
//        System.out.println(queue.peak());
//        LinkedStack stack = new LinkedStack();
//        stack.push(7);
//        stack.push(8);
//        stack.push(9);
//        System.out.println(stack.pop());
//        System.out.println(stack.pop());
//



    }







    public static void reverseList(Node head){
        Node curr = head;
        Node previous = null;
        Node next = null;
        while(curr != null){
            next = curr.getNext();
            curr.setNext(previous);
            previous = curr;
            curr = next;

        }
    }
}
