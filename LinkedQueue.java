
public class LinkedQueue<T> {
    Node head;
    Node tail;
public LinkedQueue(){
    head = tail = null;

    }
    public void enqueue(T value){
    Node newNode = new Node(value);
        if(head == null){
        head = tail = newNode;
        return;
        }
        else{
        tail.setNext(newNode);
        tail = newNode;
        }
    }
    public T dequeue(){
    if(head== null){
        return null;
    }
    else{
        Node temp = head;
        head = temp.getNext();
        return (T) temp.getValue();

    }
    }
    public T peak(){
    return (T) head.getValue();
    }
}
