class Node<T>{
private T value;
private Node next;
public Node(T data){
    this.setValue(data);
    this.setNext(null);
    }

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}
public class LinkedList<T> {
 Node head;
 Node tail;
 public LinkedList(){
     head = null;
     tail = null;
 }
 public Node addNode(T value){
     Node newNode = new Node(value);
     if(head == null){
         head = tail = newNode;
     }
     else{
         tail.setNext(newNode);
         tail = newNode;
     }
     return newNode;

 }
 public void printReverseList(){
     Node curr = tail;
     while(curr != null){
         System.out.println(curr.getValue());
         curr = curr.getNext();
     }
 }
 public void printList(){
     Node curr = head;
     while(curr != null){
         System.out.println(curr.getValue());
         curr = curr.getNext();

     }
 }
 public void deleteWithNodeReference(Node nodeToDelete){
     Node nextNode = nodeToDelete.getNext();
     if(nodeToDelete.getNext() != null){
         nodeToDelete.setValue(nextNode.getValue());
         nodeToDelete.setNext(nextNode.getNext());
     }
     else{
         Node curr = head;

        while(curr != null){
        if(curr.getNext().getNext() == null){
            curr.setNext(null);
        }
        curr = curr.getNext();
    }
     }
     return;
 }
 public void deleteWithValue(T Value){
     Node prev = null;
     Node curr = head;
     while(curr != null){
         if(curr.getValue().equals(Value)){
             if(prev == null) {
                 head = curr.getNext();
                 curr.setNext(null);
             }
             else{
                 prev.setNext(curr.getNext());

             }
             return;
         }
         prev = curr;
         curr = curr.getNext();
     }
 }
}
